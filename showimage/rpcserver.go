package main

import (
	"net"
	"net/rpc"
)

type Worker int

type ArgRpc struct {
	Names []string
}

var names []string
var imgIdx int

func (s *Worker) SetPath(args ArgRpc, reply *int) error {
	*reply = 1
	//fmt.Println(args.Name)
	if len(args.Names) == 0 {
		return nil
	}
	names = args.Names
	imgIdx = 0
	fname.Set(args.Names[0])
	return nil
}

func pathWaiter() error {
	worker1 := new(Worker)
	rpc.Register(worker1)
	l, e := net.Listen("tcp", "127.0.0.1:12336")
	if e != nil {
		return e
	}
	defer l.Close()
	//fmt.Println("start rpc")
	rpc.Accept(l)
	//fmt.Println("stop rpc")

	return nil
}

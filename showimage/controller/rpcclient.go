package main

import (
	"flag"
	"log"
	"net/rpc"
)

type Args struct {
	Name string
}
type Worker int

func main() {
	var client *rpc.Client
	var err error
	flag.Parse()
	if flag.Arg(0)=="" {
	    panic("错误参数")
	}
	
	client, err = rpc.Dial("tcp","127.0.0.1:12336")
	if err != nil {
		log.Fatal("dialing:", err)
	}
	defer client.Close()
	var ret int
	var arg Args
	arg.Name = flag.Arg(0)
	//log.Println("call Worker.SetPath")
	client.Call("Worker.SetPath",arg,&ret)
	//log.Println("end Worker.SetPath")
}

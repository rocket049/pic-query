package main

import (
	"net/rpc"
	"os"
	"os/exec"
	"path/filepath"
	"sync"

	"runtime"
)

type Args struct {
	Names []string
}
type Worker int

func showImage(names []string) error {
	var client *rpc.Client
	var err error

	client, err = rpc.Dial("tcp", "127.0.0.1:12336")
	if err != nil {
		return err
	}
	defer client.Close()
	var ret int
	var arg Args
	arg.Names = names
	//log.Println("call Worker.SetPath")
	err = client.Call("Worker.SetPath", arg, &ret)
	//log.Println("end Worker.SetPath")
	return err
}

var once sync.Once

func startViewer() {
	once.Do(func() {
		go func() {
			for {
				wd, err := os.Getwd()
				if err != nil {
					return
				}
				exe := filepath.Join(wd, "showimage")
				if runtime.GOOS == "windows" {
					exe = exe + ".exe"
				}

				cmd := exec.Command(exe)
				cmd.Run()
				cmd.Wait()
			}

		}()
	})
}
